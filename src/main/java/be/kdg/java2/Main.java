package be.kdg.java2;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");
        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:hsqldb:file:databases/students");
           // PreparedStatement preparedStatement
           //         = connection.prepareStatement("SELECT * FROM users where username = ?");
            PreparedStatement preparedStatement
                    = connection.prepareStatement("SELECT * FROM STUDENTS");
            // Statement statement = connection.createStatement();
            String username;
            //ResultSet resultSet = statement.executeQuery(
            //        "SELECT * FROM USERS WHERE USERNAME = '" + username + "'");
            //preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                double length = resultSet.getDouble("LENGTH");
                System.out.println(id + " " + name + " " + length);
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
